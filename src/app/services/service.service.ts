import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Personaje } from '../model/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(
    private _http:HttpClient
  ) {
   }

   
  getUrlAPI(){
    let url='https://breakingbadapi.com/api/characters';
    return this._http.get<any>(url)

                        /*.toPromise()
                        .then((response)=>{
                          console.log(response)
                        })
                        .catch((err)=>{
                          console.log(err)
                        })  */  
  }
}

