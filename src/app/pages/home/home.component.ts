import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import { Personaje }  from 'src/app/model/interfaces'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public nombreUsuario:string='';
  public edad:number=0;

  public personajesBB:Personaje[]= {} as Personaje[];
  
  public ejemploFor=[
    {id:1,name:'C#'},
    {id:2,name:'JS'},
    {id:3,name:'CSS'},
    {id:4,name:'HTML'},
    {id:5,name:'TypeScript'},
    {id:6,name:'Java'},
    {id:7,name:'SpringBoot'},
    {id:8,name:'Git'},
  ];

  constructor(
    private _service:ServiceService
  ) {}

  ngOnInit(): void {
    this.cargarInfoService()  
    console.log(this.ejemploFor)
  }

  cargarInfoService(){
    this._service.getUrlAPI()
      .toPromise()
      .then(req=>{
       this.personajesBB=req
        
      
      })
      .catch(err=>{
        console.log("🚀 ~ file: home.component.ts ~ line 29 ~ HomeComponent ~ cargarInfoService ~ err", err)
        
      })
  }

  saludar(){
    this.nombreUsuario="Carlos"
    this.edad=34
  }

  upEdad(){
    this.edad++;
  }
  
  DownEdad(){
    this.edad--;
  }

}
